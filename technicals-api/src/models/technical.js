const err = require('../utils/errorHandler');

function Init(symbol) {
  if (typeof symbol !== 'string' || symbol === '') err.throwErrorCode(0);

  return symbol;
}

exports.Init = Init;

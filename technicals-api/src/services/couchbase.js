const timeout = 500; // TODO : move this to internal config

/**
 * fetchTickers -
 */
async function fetchTickers(c) {
  let results = [];

  try {
    const bucket = c.bucket('stock_list'); // eslint-disable-line no-unused-vars

    const queryString = `
    SELECT ticker FROM \`stock_list\`
  `;
    const data = await c.query(queryString);

    results = data.rows.map((d) => d.ticker);
  } catch (error) {
    console.log('CB fetchTickers error', error);
  }

  return results;
}

async function writeTechnicalRecord(c, technical) {
  try {
    const bucket = c.bucket('technicals');
    const collection = bucket.defaultCollection();

    await collection.upsert(technical.symbol, technical, { timeout });
  } catch (error) {
    console.log('Technicals saving error: ', error);
  }
}

exports.fetchTickers = fetchTickers;
exports.writeTechnicalRecord = writeTechnicalRecord;

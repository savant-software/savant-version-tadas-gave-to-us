const axios = require('axios');

const fetch = (ticker, multiplier) => {
  let timeSeries = 'minute';
  let time = multiplier;

  const datetime = new Date();
  const today = datetime.toISOString().slice(0, 10);

  const buffer = 7;

  const dateFrom = datetime.setDate(datetime.getDate() - buffer);
  const dateTo = today;

  const apiKey = 'AK1EVWU6B3MHBM591OSS';

  if (multiplier === 60) {
    time = 1;
    timeSeries = 'hour';
  }

  const url = `https://api.polygon.io/v2/aggs/ticker/${ticker}/range/${time}/${timeSeries}/${dateFrom}/${dateTo}?apiKey=${apiKey}`;

  console.log('PINGED URL - ', url);

  return axios.get(url)
    .then((res) => (res.data));
};

const fetchSingle = (ticker, multiplier) => {
  let timeSeries = 'minute';
  let time = multiplier;

  const datetime = new Date();
  const today = datetime.toISOString().slice(0, 10);


  const dateFrom = today;
  const dateTo = today;

  const apiKey = 'AK1EVWU6B3MHBM591OSS';

  if (multiplier === 60) {
    time = 1;
    timeSeries = 'hour';
  }

  const url = `https://api.polygon.io/v2/aggs/ticker/${ticker}/range/${time}/${timeSeries}/${dateFrom}/${dateTo}?apiKey=${apiKey}`;

  console.log('PINGED URL - ', url);

  return axios.get(url)
    .then((res) => (res.data));
};

exports.fetch = fetch;
exports.fetchSingle = fetchSingle;

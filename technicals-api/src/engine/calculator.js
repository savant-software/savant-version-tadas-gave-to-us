const RSI = require('technicalindicators').RSI; // eslint-disable-line
const MACD = require('technicalindicators').MACD; // eslint-disable-line
const VWAP = require('technicalindicators').VWAP; // eslint-disable-line
const technicalIndicators = require('technicalindicators');

technicalIndicators.setConfig('precision', 99);

const Calculate = (data) => {
  // console.log(data.results[data.results.length - 1]);
  const closePrices = [];
  const openPrices = [];
  const highPrices = [];
  const lowPrices = [];
  const volumes = [];


  // Push each price type to corresponsing arrays
  data.results.forEach((b) => { // TODO : destructure this better
    closePrices.push(b.c);
    openPrices.push(b.o);
    highPrices.push(b.h);
    lowPrices.push(b.l);
    volumes.push(b.v);
  });

  const macdInput = {
    values: closePrices,
    fastPeriod: 12,
    slowPeriod: 26,
    signalPeriod: 9,
    SimpleMAOscillator: false,
    SimpleMASignal: false,
  };


  const macdValue = MACD.calculate(macdInput);
  const rsiValue = RSI.calculate({ period: 14, values: closePrices });

  return {
    macd: macdValue,
    rsi: rsiValue,
  };
};

const CalculateVWAP = (data) => {
  const closePrices = [];
  const openPrices = [];
  const highPrices = [];
  const lowPrices = [];
  const volumes = [];


  // Push each price type to corresponsing arrays
  data.results.forEach((b) => { // TODO : destructure this better
    closePrices.push(b.c);
    openPrices.push(b.o);
    highPrices.push(b.h);
    lowPrices.push(b.l);
    volumes.push(b.v);
  });

  const vwapInput = {
    open: openPrices,
    high: highPrices,
    low: lowPrices,
    close: closePrices,
    volume: volumes,
  };

  const vwap = new VWAP({
    open: [],
    high: [],
    low: [],
    close: [],
    volume: [],
  });

  const vwapResults = [];

  vwapInput.close.forEach((price, index) => {
    const result = vwap.nextValue({
      open: vwapInput.open[index],
      high: vwapInput.high[index],
      low: vwapInput.low[index],
      close: vwapInput.close[index],
      volume: vwapInput.volume[index],
    });
    if (result !== undefined) {
      vwapResults.push(result);
    }
  });


  return vwapResults;
};

exports.Calculate = Calculate;
exports.CalculateVWAP = CalculateVWAP;

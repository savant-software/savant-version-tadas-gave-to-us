/*
  MACD, RSI, VWAP
  Only enter a position if the MACD is higher than the MACD_signal number on all of the following time-series. 5M, 15M, 1HR
  Only enter if RSI is above 50 on 15M
  Only enter if the price is above VWAP
*/

// time series: 1Min, 5Min, 15Min, 1h
// from Alpaca docs: minute, 1Min, 5Min, 15Min, day or 1D.
// minute is an alias of 1Min. Similarly, day is of 1D.


// stock list: AAPL, MSFT, GE, F, FCSMF, SDLPF

// RSI - closing price
// EMA - closing price
// MACD - closing price
// VWAP - multi
const localFile = require('fs');
const cb = require('couchbase');

const couchbase = require('./services/couchbase');
const polygon = require('./services/polygon');


const Calculator = require('./engine/calculator');
const Technical = require('./models/technical');


// TODO : add connection to CB and pull tickers from there every run

const calculate = async (c) => {
  const date = new Date();
  console.log(`Calculating technicals...\n${date}\n\n`);

  const tickers = await couchbase.fetchTickers(c);
  console.log(`TICKERS IN CB\n${tickers}\n\n`);

  tickers.forEach(async (t) => {
    try {
      const symbol = Technical.Init(t);

      // TODO : move this somewhere
      const technical = {
        symbol,
        macd: {
          t1: {
            value: 0,
            signal: 0,
          },
          t2: {
            value: 0,
            signal: 0,
          },
          t3: {
            value: 0,
            signal: 0,
          },
        },
        rsi: {
          t1: {
            value: 0,
          },
          t2: {
            value: 0,
          },
          t3: {
            value: 0,
          },
        },
        vwap: {
          t1: {
            value: 0,
          },
          t2: {
            value: 0,
          },
          t3: {
            value: 0,
          },
        },
      };

      // take LAST result of each time series
      await polygon.fetch(symbol, 5)
        .then((technicalData) => {
          const results = Calculator.Calculate(technicalData);
          const lastMacd = results.macd[results.macd.length - 1] || {};
          const lastRsi = results.rsi[results.rsi.length - 1] || {};

          console.log('\n------------\n| 5 MINUTE |\n------------\n');
          console.log('Latest MACD - \n', lastMacd);
          console.log('Latest RSI - ', lastRsi);

          technical.macd.t1.value = lastMacd.MACD || undefined;
          technical.macd.t1.signal = lastMacd.signal || undefined;
        });
      await polygon.fetchSingle(symbol, 5)
        .then((technicalData) => {
          const results = Calculator.CalculateVWAP(technicalData);
          const lastVwap = results[results.length - 1] || {};
          console.log('Latest VWAP - ', lastVwap, '\n');

          technical.vwap.t1.value = lastVwap || undefined;

          console.log('--------------------------------\n\n');
        });

      await polygon.fetch(symbol, 15)
        .then((technicalData) => {
          const results = Calculator.Calculate(technicalData);
          const lastMacd = results.macd[results.macd.length - 1] || {};
          const lastRsi = results.rsi[results.rsi.length - 1] || {};

          console.log('\n------------\n| 15 MINUTE |\n------------\n');
          console.log('Latest MACD - \n', lastMacd);
          console.log('Latest RSI - ', lastRsi);

          technical.macd.t2.value = lastMacd.MACD || undefined;
          technical.macd.t2.signal = lastMacd.signal || undefined;
          technical.rsi.t2.value = lastRsi || undefined;
        });
      await polygon.fetchSingle(symbol, 15)
        .then((technicalData) => {
          const results = Calculator.CalculateVWAP(technicalData);
          const lastVwap = results[results.length - 1] || {};
          console.log('Latest VWAP - ', lastVwap, '\n');

          technical.vwap.t2.value = lastVwap || undefined;

          console.log('--------------------------------\n\n');
        });

      await polygon.fetch(symbol, 60)
        .then((technicalData) => {
          const results = Calculator.Calculate(technicalData);
          const lastMacd = results.macd[results.macd.length - 1] || {};
          const lastRsi = results.rsi[results.rsi.length - 1] || {};

          console.log('\n------------\n| 60 MINUTE |\n------------\n');
          console.log('Latest MACD - \n', lastMacd);
          console.log('Latest RSI - ', lastRsi);

          technical.macd.t3.value = lastMacd.MACD || undefined;
          technical.macd.t3.signal = lastMacd.signal || undefined;
          technical.rsi.t3.value = lastRsi || undefined;
        });
      await polygon.fetchSingle(symbol, 60)
        .then((technicalData) => {
          const results = Calculator.CalculateVWAP(technicalData);
          const lastVwap = results[results.length - 1] || {};
          console.log('Latest VWAP - ', lastVwap, '\n');

          technical.vwap.t3.value = lastVwap || undefined;

          console.log('--------------------------------\n\n');
        });


      couchbase.writeTechnicalRecord(c, technical);
    } catch (error) {
      console.log(`EXCEPTION!\n${error}\n\n`); // TODO : move logging to central location LOCAL ERRORS + CONSOLE
    }
  });
};

const init = (config) => {
  /**
   * createCluster - initiate a connection with couchbase
   */
  const c = new cb.Cluster(
    'couchbase://localhost',
    // {
    //   username: 'adminlocal',
    //   password: 'password',
    // },
    {
      username: 'Administrator',
      password: 'S4v4nt123!',
    },
  );

  try {
    calculate(c);
    setInterval(() => {
      calculate(c);
    }, 60000);
  } catch (err) {
    console.error(err);
  }
};

exports.init = init;

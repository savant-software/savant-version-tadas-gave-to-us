const errorMap = require('../lang/errors');

/**
 * throwError - helper for throwing errors
 * @param {string} msg - error message
 */
const throwError = (msg) => {
  throw new Error(msg);
};

/**
 * throwErrorCode - helper for throwing errors that locates error strings from a map and lang file
 * @param {number} errorCode - the error number from lang/errors
 */
const throwErrorCode = (errorCode) => {
  const message = errorMap.messages[errorCode];
  throw new Error(message);
};

exports.throwError = throwError;
exports.throwErrorCode = throwErrorCode;

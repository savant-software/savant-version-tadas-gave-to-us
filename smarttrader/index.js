const config = require('./src/constants/config');
const Engine = require('./src/main');

Engine.init(config);

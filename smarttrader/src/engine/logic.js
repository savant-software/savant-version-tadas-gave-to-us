const slack = require('../services/slack');
const errorMap = require('../lang/errors');
const err = require('../utils/errorHandler');
const Logger = require('../utils/logger');

const logicCircuit = (rawStock, orderRecord, technicalData) => {
  const stock = rawStock.getJson();
  const { price, symbol } = stock;

  let trade = false;

  if (orderRecord) { // technical check could be problematic for closed orders that exist in CB
    Logger.logMessage(stock, `ORDER DETECTED FOR - ${symbol}\nChecking Resistance Logic + Technicals`);

    if (parseFloat(technicalData.macd.t2.value) < parseFloat(technicalData.macd.t2.signal)) {
      Logger.logMessage(stock, `TECHNICAL STOP LOSS TRIGGERED FOR - ${symbol} - MACD T2`, true, {});
      stock.technicalStopLoss = true;
      return stock;
    }
    if (parseFloat(technicalData.rsi.t2.value) < 55.0) {
      Logger.logMessage(stock, `TECHNICAL STOP LOSS TRIGGERED FOR - ${symbol} - RSI T2`, true, {});
      stock.technicalStopLoss = true;
      return stock;
    }
    if (parseFloat(price) <= parseFloat(technicalData.vwap.t1.value)) {
      Logger.logMessage(stock, `TECHNICAL STOP LOSS TRIGGERED FOR - ${symbol} - VWAP T1`, true, {});
      stock.technicalStopLoss = true;
      return stock;
    }
    if (parseFloat(price) <= parseFloat(technicalData.vwap.t2.value)) {
      Logger.logMessage(stock, `TECHNICAL STOP LOSS TRIGGERED FOR - ${symbol} - VWAP T2`, true, {});
      stock.technicalStopLoss = true;
      return stock;
    }

    Logger.logMessage(stock, `TECHNICAL STOP LOSS NOT TRIGGERED FOR - ${symbol}`, true, {});
    Logger.logMessage(stock, 'CHECKING FOR RESISTANCE INCREASE');

    const or = orderRecord.resistance;
    const sr = stock.resistance;
    if (((sr - or) / (sr + or)) * 100 < 5.0) {
      slack.sendWebhook(false, errorMap.messages[1], stock);
      err.throwErrorCode(1);
    }
    // adjust stop loss to old order resistance value - 1%
    const adjustedStopLoss = (or - (or * 0.01));
    stock.signalShort = adjustedStopLoss;

    const decorator = '```';
    const details = (
      `${decorator}
        Initial Entry Values\n
        SignalN: ${orderRecord.signalN}\n
        SignalP: ${orderRecord.signalP}\n
        Resistance: ${orderRecord.resistance}\n
        \n
        New Value:\n
        Resistance: ${stock.resistance}
      ${decorator}`
    );
    const msg = (
      `:exclamation: Resistance has been increased for - ${symbol}\n\n :exclamation:
        ${details}
      `
    );
    slack.resistanceAlert(msg);
    return stock;
  }

  // if Max property is not over 5%, back out
  if (parseFloat(stock.max) < 5.0) {
    slack.sendWebhook(false, errorMap.messages[2], stock);
    err.throwErrorCode(2);
  }

  // check if Resistance is 5% greater than Long Signal
  const ar = stock.adjustedResistance;
  const sl = stock.signalLong;
  if (((ar - sl) / ((ar + sl) / 2)) * 100.0 < 5.0) {
    slack.sendWebhook(false, errorMap.messages[3], stock);
    err.throwErrorCode(3);
  }

  // check technical data
  if (
    parseFloat(technicalData.macd.t1.value) < parseFloat(technicalData.macd.t1.signal)
    || parseFloat(technicalData.macd.t2.value) < parseFloat(technicalData.macd.t2.signal)
    || parseFloat(technicalData.macd.t3.value) < parseFloat(technicalData.macd.t3.signal)
  ) {
    slack.sendWebhook(false, errorMap.messages[4], stock);
    err.throwErrorCode(4);
  }
  if (parseFloat(technicalData.rsi.t2.value) < 55.0) {
    slack.sendWebhook(false, errorMap.messages[5], stock);
    err.throwErrorCode(5);
  }
  if (parseFloat(price) < parseFloat(technicalData.vwap.t1.value)) {
    slack.sendWebhook(false, errorMap.messages[6], stock);
    err.throwErrorCode(6);
  }
  Logger.logMessage(stock, `TRADE LOGIC PASSED FOR ${stock.symbol}`, true);
  trade = true;

  return (trade ? stock : null);
};

exports.logicCircuit = logicCircuit;

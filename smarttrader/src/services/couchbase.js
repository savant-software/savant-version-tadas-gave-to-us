
const Couchbase = require('couchbase');
const Logger = require('../utils/logger');

// TODO - move this to config / env vars
const COUCHBASE_INSTANCE = new Couchbase.Cluster(
  'couchbase://localhost',
  // {
  //   username: 'adminlocal',
  //   password: 'password',
  // },
  {
    username: 'Administrator',
    password: 'S4v4nt123!',
  },
);

const init = () => (COUCHBASE_INSTANCE);

const timeout = 1000; // TODO : move this to internal config

/**
 * updateStockList - used for technicals
 * @param {*} symbol
 */
function updateStockList(c, stock) {
  try {
    const bucket = c.bucket('stock_list');
    const collection = bucket.defaultCollection();

    const data = {
      active: stock.getValue('activeTrade'),
      ticker: stock.getValue('symbol'),
    };

    collection.upsert(stock.getValue('symbol'), data, { timeout });
  } catch (error) {
    Logger.logMessage(stock.getValue('symbol'), `CB updateStockList error: ${stock.getValue('symbol')}\n${error}`, true, error);
  }
}

/**
 * writeHistoryRecord -
 * @param {Stock} stock -
 */
function writeHistoryRecord(c, stock) {
  try {
    const bucket = c.bucket('history');
    const collection = bucket.defaultCollection();

    collection.upsert(stock.getValue('symbol'), stock.getJson(), { timeout });
  } catch (error) {
    Logger.logMessage(stock.getJson(), `CB writeHistoryRecord error: ${stock.getValue('symbol')}\n${error}`, true, error);
  }
}

/**
 * getOrderRecord -
 * @param {string} symbol -
 */
async function getOrderRecord(c, symbol) {
  const bucket = c.bucket('orders_placed'); // eslint-disable-line no-unused-vars
  const collection = bucket.defaultCollection();

  const opts = {
    timeout,
  };

  try {
    const result = await collection.get(symbol, opts);
    return result;
  } catch (error) {
    Logger.logMessage({ stock: symbol }, `CB getOrderRecord error: ${symbol}`, false, error);
    return null;
  }
}

/**
 * eraseOrderRecord -
 * @param {string} symbol -
 */
async function eraseOrderRecord(c, symbol) {
  const bucket = c.bucket('orders_placed'); // eslint-disable-line no-unused-vars
  const collection = bucket.defaultCollection();

  const opts = {
    timeout,
  };

  try {
    const result = await collection.remove(symbol, opts);
    return result;
  } catch (error) {
    Logger.logMessage({ stock: symbol }`CB eraseOrderRecord error: ${symbol}`, true, error);
    return null;
  }
}

/**
 * writeOrderRecord -
 * @param {object} stock -
 */
async function writeOrderRecord(c, stock) {
  const newStock = { ...stock };
  try {
    await getOrderRecord(c, newStock.symbol).then(async (response) => {
      if (response && response.rows && response.rows.length > 0) {
        newStock.timesOrdered = response.rows[0].orders_placed.timesOrdered + 1;
      } else {
        newStock.timesOrdered = 1;
      }

      newStock.activeTrade = true;
      const bucket = c.bucket('orders_placed');
      const collection = bucket.defaultCollection();

      await collection.upsert(stock.symbol, stock, { timeout });
    });
  } catch (error) {
    Logger.logMessage(stock.getJson(), `CB writeOrderRecord error: ${stock.getValue('symbol')}\n${error}`, true, error);
  }
  return null;
}

/**
 * getOrderRecord -
 * @param {string} symbol -
 */
async function getTechnicalRecord(c, symbol) {
  const bucket = c.bucket('technicals'); // eslint-disable-line no-unused-vars
  const collection = bucket.defaultCollection();

  const opts = {
    timeout,
  };
  try {
    const result = await collection.get(symbol, opts);
    return result;
  } catch (error) {
    Logger.logMessage({ stock: symbol }`CB getTechnicalRecord error: ${symbol}\n${error}`, true, error);
    return null;
  }
}

exports.init = init;
exports.updateStockList = updateStockList;
exports.writeHistoryRecord = writeHistoryRecord;
exports.getOrderRecord = getOrderRecord;
exports.eraseOrderRecord = eraseOrderRecord;
exports.writeOrderRecord = writeOrderRecord;
exports.getTechnicalRecord = getTechnicalRecord;

const Alpaca = require('@alpacahq/alpaca-trade-api');
const couchbase = require('./couchbase');
const logic = require('../engine/logic');
const Logger = require('../utils/logger');
const slack = require('./slack');

// TODO - move this to config / env vars
const ALPACA_INSTANCE = new Alpaca({
  keyId: 'PKSKXQSC08SRLP96DI52',
  secretKey: 'up8R5ECOtR8bq6k7rJ5izYjS1XV34ReMEGAjg9ke',
  paper: true,
  usePolygon: true,
});

const init = () => (ALPACA_INSTANCE);

const getAccount = async () => {
  const acc = await ALPACA_INSTANCE.getAccount();
  return acc;
};

const technicalStopLoss = async (symbol) => {
  const orders = await ALPACA_INSTANCE.getOrders({ status: 'open' })
    .then((o) => o.filter((match) => match.symbol === symbol))
    .catch((error) => Logger.logMessage({}, `ERROR GETTING ORDERS WHILE TRYING TO CANCEL - ${symbol}`, true, error));
  if (orders.length > 1) Logger.logMessage({}, `SUCCESSFULLY FETCHED ORDERS FOR ${symbol}`, true, orders);

  await orders.forEach((o) => {
    ALPACA_INSTANCE.cancelOrder(o.id)
      .then((resp) => Logger.logMessage({}, `SUCCESSFULLY CANCELLED ORDER (${o.id}) FOR ${symbol}`, true, resp))
      .catch((error) => Logger.logMessage({}, `ERROR CANCELLING ORDER (${o.id}) FOR ${symbol}`, true, error));
  });
};

const closeAllPositions = async () => {
  await ALPACA_INSTANCE.cancelAllOrders().then(
    ALPACA_INSTANCE.closeAllPositions(),
  );
};

const closePosition = async (symbol) => {
  await ALPACA_INSTANCE.closePosition(symbol)
    .then((resp) => Logger.logMessage({}, `SUCCESSFULLY CLOSED POSITION FOR (${symbol})`, true, resp))
    .catch((error) => Logger.logMessage({}, `ERROR CLOSING POSITION - ${symbol}`, true, error));
};

const sendOrder = async (stock, purchasePower) => {
  const type = stock.lastPrice < stock.signalLong ? 'limit' : 'market';
  const stop_loss = { // eslint-disable-line camelcase
    stop_price: stock.signalShort,
  };
  if (type === 'limit') {
    stop_loss.limit_price = stock.signalShort;
  }

  await ALPACA_INSTANCE.createOrder({
    side: 'buy',
    symbol: stock.symbol,
    type,
    qty: Math.round(purchasePower / stock.lastPrice),
    time_in_force: 'gtc',
    order_class: 'bracket',
    take_profit: {
      limit_price: stock.adjustedResistance,
    },
    stop_loss,
  });
};

const smartTrade = async (alpaca, account, couchbaseInstance, stock) => {
  const { buying_power, last_equity } = account; // eslint-disable-line camelcase
  const symbol = stock.getValue('symbol');

  // TODO - pass these two calls through a model to validate and destructure
  let technicalData;
  await couchbase.getTechnicalRecord(couchbaseInstance, symbol).then(
    (t) => {
      if (t && t.value) technicalData = t.value;
    },
  );
  let orderRecord;
  await couchbase.getOrderRecord(couchbaseInstance, symbol).then(
    (o) => {
      if (o && o.value) orderRecord = o.value;
    },
  );

  const adjustedStock = await logic.logicCircuit(stock, orderRecord, technicalData);

  if (adjustedStock) {
    if (adjustedStock.technicalStopLoss) {
      technicalStopLoss(symbol).then(
        (resp) => {
          Logger.logOrder(adjustedStock, technicalData, `TECHNICAL STOP LOSS TRIGGERED - ${symbol}`, true, resp);
          couchbase.eraseOrderRecord(couchbaseInstance, adjustedStock.symbol);
          slack.sendWebhook(true, '+Technical Stoploss Triggered+', adjustedStock);
        },
      ).catch((error) => Logger.logMessage(adjustedStock, `ERROR CHANGING ORDER TO MARKET SELL - ${symbol}`, true, error));
    } else {
      // use daily buying power / 4
      // (25% to each possible stock) TODO : make this number dynamically adjust
      const buyingPower = Math.round(last_equity / 4); // eslint-disable-line camelcase
      sendOrder(adjustedStock, buyingPower).then(
        (resp) => {
          Logger.logOrder(adjustedStock, technicalData, `SENDING ORDER - ${symbol}`, true, resp);
          couchbase.writeOrderRecord(couchbaseInstance, adjustedStock);
          slack.sendWebhook(true, '+Succesful Trade!+', adjustedStock);
        },
      ).catch((error) => Logger.logMessage(adjustedStock, `ERROR SENDING ORDER FOR - ${adjustedStock.symbol}`, true, error)); // TODO : move to central logging location
    }
  }
};

exports.init = init;
exports.getAccount = getAccount;
exports.closePosition = closePosition;
exports.closeAllPositions = closeAllPositions;
exports.smartTrade = smartTrade;

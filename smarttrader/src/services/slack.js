const Slack = require('slack-node');

// TODO : hide secrets
const defaultWebhook = 'https://hooks.slack.com/services/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';
const technicalsWebhook = 'https://hooks.slack.com/services/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';
const resistanceWebhook = 'https://hooks.slack.com/services/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';

const slack = new Slack();

/**
 * 
 */
const sayHello = () => {
  const date = new Date();
  const month = date.getMonth();
  const day = date.getDate();
  const hours = date.getHours();
  const min = date.getMinutes();

  slack.setWebhook(defaultWebhook);
  slack.webhook({
    username: 'SmartTrader',
    channel: '#server-hooks',
    iconEmoji: ':robot_face:',
    text: `:robot_face: *SmartTrader Bot Initiated.* :wave:\n\n${month}/${day} | ${hours}:${min}`,
  },
  (err, response) => {
    // console.log(response); // TODO : move logging to central location
  });
};

/**
 * 
 * @param {*} purchased 
 * @param {*} msg 
 * @param {*} stock 
 */
const sendWebhook = (purchased, msg, stock) => {
  let emoji = ':moneybag:';
  if (!purchased) emoji = ':exclamation:';

  const basicValues = `Max: ${stock.max}`;
  const entryValues = (
    `Entry Values\n
    SignalN: ${stock.signalN}\n
    SignalP: ${stock.signalP}\n
    Resistance: ${stock.resistance}`
  );
  const calculatedValues = (
    `Adjusted Values\n
    Signal Short: ${stock.signalShort}\n
    Signal Long: ${stock.signalLong}\n
    Resistance: ${stock.adjustedResistance}
    `
  );
  const rrRatio = (
    (
      (stock.adjustedResistance - stock.signalLong)
      / ((stock.adjustedResistance + stock.signalLong) / 2)
    ) * 100) / 100;
  const riskReward = `Risk / Reward Ratio: ${rrRatio}`;

  const screenshotUrl = `https://ocr-screenshots.s3.amazonaws.com/${stock.location}`;

  const separator = '-----------------------------------';
  const decorator = '```';

  const body = (
    `${decorator}
      ${basicValues}\n
      \n
      ${entryValues}\n
      \n
      ${calculatedValues}\n
      \n
      ${riskReward}
    ${decorator}\n
    Screenshot URL: ${screenshotUrl}\n
    ${separator}
    `
  );

  slack.setWebhook(defaultWebhook);
  slack.webhook({
    username: 'SmartTrader',
    channel: '#server-hooks',
    iconEmoji: ':robot_face:',
    text: (
      `\n${emoji} *${msg} - ${stock.symbol}* ${emoji}
      ${body}
      `
    ),
  },
  (err, response) => {
    // console.log(response); // TODO : move logging to central location LOCAL ERRORS
  });
};

/**
 * 
 * @param {*} msg 
 */
const resistanceAlert = (msg) => {
  slack.setWebhook(resistanceWebhook);
  slack.webhook({
    username: 'SmartTrader',
    channel: '#resistance-changes',
    iconEmoji: ':robot_face:',
    text: `\n${msg}`,
  },
  (err, response) => {
    // console.log(response); // TODO : move logging to central location LOCAL ERRORS
  });
};

exports.sayHello = sayHello;
exports.sendWebhook = sendWebhook;
exports.resistanceAlert = resistanceAlert;

const dgram = require('dgram');
const err = require('../utils/errorHandler');

/**
 * InputStream - listens to port configured for OCR / stock data input
 * @param {*} config
 */
function InputStream(config) {
  const { port } = config;
  const server = dgram.createSocket({ type: 'udp4', reuseAddr: true });

  server.bind(port);

  server.on('error', (error) => {
    err.throwError(`server error:\n${error.stack}`);
    server.close();
  });

  // TODO : send hello message to slack and wherever else
  server.on('listening', () => {
    const address = server.address();
    console.log(`server listening ${address.address}:${address.port}`); // TODO : move loggin to central location
  });

  return server;
}

exports.InputStream = InputStream;

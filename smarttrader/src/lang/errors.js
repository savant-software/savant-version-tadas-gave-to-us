exports.messages = {
  0: 'No stock data. Most likely an issue with OCR',
  1: 'Resistance has not increased enough to re-order',
  2: 'Max logic not satisfied',
  3: 'Risk / Reward logic not satisfied',
  4: 'MACD logic not satisfied',
  5: 'RSI logic not satisfied',
  6: 'VWAP logic not satisfied',
  7: 'Could not find technicals data for stock',
  8: 'Undefined value encountered in technicals data',
};

// Copyright (C) 2019-2021 Savant, LLC - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential, all files owned by Savant, LLC
// Software Contact Michael Esparza, <michael@savant.software>

const err = require('../utils/errorHandler');

/**
 * A representation of a single Stock
 * @param {*} data - raw input data to be parsed into JSON
 */
function Init(data) {
  // TODO : Parsing error handling
  const parsed = JSON.parse(data);

  // TODO : set default values so that types are known, then map over data and change values
  const properties = {
    activeTrade: false,
    max: parsed.Max || 0.0,
    capturedAt: parsed.CapturedAt || '',
    location: parsed.Location || '',
    resistance: parsed.Resistance || 0.0,
    signalN: parsed.SignalN || 0.0,
    signalP: parsed.SignalP || 0.0,
    signalShort: undefined,
    signalLong: undefined,
    adjustedResistance: undefined,
    timesOrdered: 0,
    symbol: parsed.Symbol || '',
    price: parsed.Last || 0.0,
    technicalStopLoss: false,
    lastPrice: parsed.Last || 0.0,
  };

  // TODO : move these values to config?
  const signalShortAdjustment = 0.995;
  const signalLongAdjustment = 1.005;
  const resistanceAdjustment = 0.99;

  properties.signalShort = properties.signalN * signalShortAdjustment;
  properties.signalLong = properties.signalP * signalLongAdjustment;
  properties.adjustedResistance = properties.resistance * resistanceAdjustment;

  if (typeof properties.symbol !== 'string' || properties.symbol === '') err.throwErrorCode(0);

  return {
    getJson: () => (properties),

    /**
     * changes a properties value
     * @param {string} property - the name of the property to be changed
     * @param {string} newValue - the new value
     */
    changeValue: (property, newValue) => {
      properties[property] = newValue;
    },

    /**
     * returns a specific property
     * @param {string} property - "symbol" or "longSignal" etc
     */
    getValue: (property) => properties[property],
  };
}

exports.Init = Init;

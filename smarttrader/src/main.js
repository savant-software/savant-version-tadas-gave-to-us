// Copyright (C) 2019-2021 Savant, LLC - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential, all files owned by Savant, LLC
// Software Contact Michael Esparza, <michael@savant.software>

const fs = require('fs');

const Alpaca = require('./services/alpaca');
const Couchbase = require('./services/couchbase');
const Socket = require('./services/socket');
const Slack = require('./services/slack');
const Logger = require('./utils/logger');

const Stock = require('./models/stock');

/**
 * Central controller for SmartTrader operations
 * @param {Object} config - env var filled config file used throughout SmartTrader to
 *                          differentiate running for testing vs. live trading
 */
const init = async (config) => {
  Slack.sayHello();
  Logger.init();

  const alpaca = await Alpaca.init();
  const account = await Alpaca.getAccount();
  const couchbaseInstance = await Couchbase.init();
  const DataListener = Socket.InputStream(config.socket);


  DataListener.on('message', (msg /* , rinfo */) => {
    // console.log(`Server receieved data:\n ${msg}\n\n from\n\n ${rinfo.address}:${rinfo.port}`);

    try {
      const stock = Stock.Init(msg);
      Couchbase.writeHistoryRecord(couchbaseInstance, stock);
      Couchbase.updateStockList(couchbaseInstance, stock);

      Alpaca.smartTrade(alpaca, account, couchbaseInstance, stock).catch(
        (error) => Logger.logMessage(stock.getJson(), `LOGIC CIRCUIT EXCEPTION - ${stock.getValue('symbol')}`, true, error), // TODO : move logging to central location LOGIC CIRCUIT
      );
    } catch (error) {
      Logger.logMessage({}, 'SYSTEM EXCEPTION!', true, error); // TODO : move logging to central location SYSTEM
    }
  });

  // TODO : move websocket connection to diff file
  const updatesClient = alpaca.websocket;

  updatesClient.onConnect(() => {
    Logger.logWebsocket('Connected', true);
    const tradeKeys = ['trade_updates', 'account_updates'];
    updatesClient.subscribe(tradeKeys);
  });

  updatesClient.onStateChange((newState) => {
    Logger.logWebsocket(`State changed to ${newState}`);
  });

  updatesClient.onDisconnect(() => {
    Logger.logWebsocket('Disconnected', true);
  });

  updatesClient.onOrderUpdate((data) => {
    Logger.logWebsocket(`Order updates: ${JSON.stringify(data, null, 2)}`, true);

    const { event, order } = data;
    const { symbol } = order;

    if (event === 'fill' || event === 'partial_fill') {
      if (data.position_qty && data.position_qty === 0) {
        // TODO : need to set stock 'activeTrade' to false here
        // maybe something like Couchbase.changeOrderRecod(couchbaseInstance, symbol)
        // Couchbase.eraseOrderRecord(couchbaseInstance, symbol);
        Logger.logMessage(symbol, 'Naturally exited trade, changing stock in CB to inactive', true, data);
      }
    }
    if (event === 'canceled') {
      Alpaca.closePosition(symbol);
    }
  });

  updatesClient.onAccountUpdate((data) => {
    Logger.logWebsocket(`Account updates: ${JSON.stringify(data, null, 2)}`, true);
  });

  updatesClient.connect();

  // Move this elsewhere as well maybe
  const now = new Date();
  let marketClose = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 13, 49, 0, 0) - now;
  if (marketClose < 0) {
    marketClose += 86400000;
  }
  setTimeout(() => {
    Alpaca.closeAllPositions().then(process.exit());
  }, marketClose);
};

exports.init = init;

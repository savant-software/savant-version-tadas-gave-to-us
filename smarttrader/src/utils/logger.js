const fs = require('fs');
const path = require('path');

const getDate = () => {
  const dateObj = new Date();
  const date = (`0${dateObj.getDate()}`).slice(-2);
  const year = dateObj.getFullYear();
  const month = (`0${dateObj.getMonth() + 1}`).slice(-2);
  const hours = dateObj.getHours();
  const minutes = dateObj.getMinutes();
  const seconds = dateObj.getSeconds();

  return [
    `${year}-${month}-${date} ${hours}:${minutes}:${seconds}`,
    `${hours}:${minutes}:${seconds}`,
    `${year}-${month}-${date}`,
  ];
};

const init = () => {
  const logsName = `logs-${getDate()[2]}.txt`;
  const orderLogsName = `orders-${getDate()[2]}.txt`;
  const polygonLogsName = `polygon-${getDate()[2]}.txt`;

  fs.stat(logsName, (error) => {
    if (error == null) {
      // file exists
    } else if (error.code === 'ENOENT') {
      fs.writeFile(logsName, `PROGRAM INITIALIZED - ${getDate()[0]}`, (err) => {
        if (err) throw err;
      });
    } else {
      throw error;
    }
  });

  fs.stat(orderLogsName, (error) => {
    if (error == null) {
      // file exists
    } else if (error.code === 'ENOENT') {
      fs.writeFile(orderLogsName, `PROGRAM INITIALIZED - ${getDate()[0]}`, (err) => {
        if (err) throw err;
      });
    } else {
      throw error;
    }
  });

  fs.stat(polygonLogsName, (error) => {
    if (error == null) {
      // file exists
    } else if (error.code === 'ENOENT') {
      fs.writeFile(polygonLogsName, `PROGRAM INITIALIZED - ${getDate()[0]}`, (err) => {
        if (err) throw err;
      });
    } else {
      throw error;
    }
  });
};

const LogMessage = (stock, msg, alert, resp) => {
  const time = getDate()[1];
  const timeStampedMsg = `\n\n${time}\n${msg}\n${JSON.stringify(stock)}\n${resp}\n---------------\n\n`;

  fs.appendFile(`logs-${getDate()[2]}.txt`, timeStampedMsg, (err) => {
    if (err) {
      return console.error(err); // eslint-disable-line no-console
    }
    if (alert) {
      console.log(`${msg}\n${resp}\n\n`); // eslint-disable-line no-console
    }
    return null;
  });
};

const LogOrder = (stock, technicalData, msg, alert, resp) => {
  const time = getDate()[1];
  const timeStampedMsg = `\n${time}\n${msg}\n${JSON.stringify(stock)}\n\nTECHNICAL DATA:\n${JSON.stringify(technicalData)}\n${JSON.stringify(resp)}\n---------------`;

  fs.appendFile(`orders-${getDate()[2]}.txt`, timeStampedMsg, (err) => {
    if (err) {
      return console.error(err); // eslint-disable-line no-console
    }
    if (alert) {
      console.log(`${msg}\n${resp}\n\n`); // eslint-disable-line no-console
    }
    return null;
  });
};

const LogWebsocket = (msg, alert) => {
  const time = getDate()[1];
  const timeStampedMsg = `\n${time}\n${msg}\n\n---------------\n`;
  fs.appendFile(`polygon-${getDate()[2]}.txt`, timeStampedMsg, (err) => {
    if (err) {
      return console.error(err); // eslint-disable-line no-console
    }
    if (alert) {
      console.log(msg); // eslint-disable-line no-console
    }
    return null;
  });
};

exports.init = init;
exports.logMessage = LogMessage;
exports.logOrder = LogOrder;
exports.logWebsocket = LogWebsocket;

# Smart Trader

Smart Trader is an automated stock trading algorithm designed by Savant Capital and implemented by Savant Software.
This API depends on a JSON data stream of stock information and uses the [Alpaca API](https://alpaca.markets) to place orders.
### Tech

* [Node] - https://nodejs.org/
* [Alpaca API] - https://alpaca.markets

### Installation

**Install Node JS via: https://nodejs.org/**
**Install Yarn via: https://yarnpkg.com/**

**Install all dependencies using Yarn**

```sh
$ yarn install
```

### To run the code from the server:

**CD into correct directory:**
```sh
$ cd C:\Users\Administrator\Repositories\savant-software\savant-core\smart-trader\
```

**Switch to correct version of Node (using NVM - Node Version Manager)**
```sh
$ nvm use
```

**Run the script:**
```sh
$ yarn start
```

 


# OCR Middleware

OCR Middleware is a Python app designed to take screenshots and process the data into JSON that is sent to a smart-trader API.
### Tech

This middleware uses a number of open source projects to work properly:

* [Python]
* [Amazon Textract] - OCR designed to process text data out of images

### Installation

OCR Middleware requires [Python](https://www.python.org/) v3.7 to run.

### To run the code from the server:

**CD into correct directory:**
```sh
$ cd C:\Users\Administrator\Repositories\savant-software\savant-core\ocr-mw
```

**Run the script:**
```sh
$  C:\Users\Administrator\AppData\Local\Programs\Python\Python38-32\python.exe src\run.py
```

**Log Files**

* The program will automatically create a `/logs` directory.
* Each day, a single `.txt` file will be created to log sent data, along with errors

**This was mainly done to minimize information being sent through PowerShell**

---


### Be sure to minimize anything blocking the view of Stockstotrade as the script will take screenshots of the entire screen.


# Copyright (C) Savant Software, LLC - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential, all files owned by Savant Software, LLC
# Software Contact Tadas Tsibulskis, <tadas@savant.capital>

import boto3
import os
import csv
import json
import io
import re
import pprint
import difflib
from io import StringIO

# TODO : Move these into Vault
os.environ["AWS_ACCESS_KEY_ID"] = "AKIAQVL7BDPUBKCUMNLL"
os.environ["AWS_SECRET_ACCESS_KEY"] = "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"
os.environ["AWS_DEFAULT_REGION"] = "us-east-1"


def get_rows_columns_map(table_result, blocks_map):
    rows = {}
    for relationship in table_result['Relationships']:
        if relationship['Type'] == 'CHILD':
            for child_id in relationship['Ids']:
                cell = blocks_map[child_id]
                if cell['BlockType'] == 'CELL':
                    row_index = cell['RowIndex']
                    col_index = cell['ColumnIndex']
                    if row_index not in rows:
                        # create new row
                        rows[row_index] = {}

                    # get the text value
                    rows[row_index][col_index] = get_text(cell, blocks_map)
    return rows

def get_text(result, blocks_map):
    text = ''
    if 'Relationships' in result:
        for relationship in result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    word = blocks_map[child_id]
                    if word['BlockType'] == 'WORD':
                        text += word['Text'] + ' '
                    if word['BlockType'] == 'SELECTION_ELEMENT':
                        if word['SelectionStatus'] == 'SELECTED':
                            text += 'X '
    return text

# extract data from image as csv
def get_csv_from_image(file_name):
    with open(file_name, 'rb') as file:
        img_test = file.read()
        bytes_test = bytearray(img_test)
        print('\nImage loaded, parsing CSV', file_name)

    # use Amazon boto3 OCR to analyze screenshot
    ocr = boto3.client('textract')
    document = ocr.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

    # pull Block array out of document object
    blocks = document['Blocks']


    blocks_map = {}
    table_blocks = []
    for block in blocks:
        blocks_map[block['Id']] = block
        if block['BlockType'] == "TABLE":
            table_blocks.append(block)

    if len(table_blocks) <= 0:
        return "<b> NO Table FOUND </b>"

    csv = ''
    for index, table in enumerate(table_blocks):
        csv += generate_table_csv(table, blocks_map, index + 1)
        csv += '\n\n'

    return csv


def clean_number_strings_convert_to_float(s):
    return float(re.sub('[^0-9]','', s))


# Helper function to convert millions to a numerical value
def replace_m_with_correct_number(input_string):
    try:
        if input_string.endswith("M") or input_string.endswith("N"):
            return str(clean_number_strings_convert_to_float(input_string) * 1000000)
        elif input_string.endswith("B"):
            return str(clean_number_strings_convert_to_float(input_string) * 10000000)
        elif input_string.endswith("K"):
            try:
                return str(clean_number_strings_convert_to_float(input_string) * 1000)
            except Exception:
                return input_string
        else:
            return input_string
    except Exception:
        return input_string


def generate_table_csv(table_result, blocks_map, table_index):
    rows = get_rows_columns_map(table_result, blocks_map)

    # get cells.
    csv = ""

    for row_index, cols in rows.items():

        for col_index, text in cols.items():
            # TODO : Break this up and use a csv encoder. gonna eventually fuck shit up
            csv += '{}'.format(replace_m_with_correct_number(text.rstrip().strip("%"))) + ","
        csv += '\n'

    csv += '\n\n\n'
    return csv

# Helper func that checks similarity of strings
def fix_header(input, expected):
    output = input
    seq = difflib.SequenceMatcher(None, input, expected)
    if seq.ratio() * 100 > 75:
        output = expected
    return output


# This needs to be fixed very ugly way of converting strings to floats
# TODO: Break this function up. its doing to much, also prune data
def convert_to_dist(str):
    is_first_row = True
    is_first_signal_key = True

    f = StringIO(str)
    reader = csv.reader(f, delimiter=',')
    headers = []
    data = []
    for row in reader:
        current_row = {}

        if is_first_row == True: # this block is a temp fix

            for column in row:
                # Right now were only fixing Signa to be Signal there will be more corrections i'm sure.
                c = fix_header(column, "Signal")
                c = fix_header(c, "Symbol")

                # Lets next get rid of anything but letters
                c = re.sub(r'[^A-Za-z]', '', c)

                # Now we need to handle the double signal key issue
                if c == "Signal" and is_first_signal_key == True:
                    c = "SignalN"
                    is_first_signal_key = False
                elif c == "Signal":
                    c = "SignalP"

                headers.append(c)

            is_first_row = False

        else:

            if len(row) == 0:
                continue

            for index, column in enumerate(row):
                if column == "":
                    continue
                # convert to a number if possible
                try:
                    new_item = float(column)
                except Exception:
                    new_item = column

                h = headers[index]
                current_row[h] = new_item
            if not current_row:
                continue
            else:
                data.append(current_row)

    return data
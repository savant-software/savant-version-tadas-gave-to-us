# Copyright (C) Savant Software, LLC - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential, all files owned by Savant Software, LLC
# Software Contact Tadas Tsibulskis, <tadas@savant.capital>

import socket
import time

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
addr = ('127.0.0.1', 10001)

def send_message(msg):
    client_socket.settimeout(1.0)
    bmsg = str.encode(msg)

    client_socket.sendto(bmsg, addr)

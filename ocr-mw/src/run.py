# Copyright (C) Savant Software, LLC - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential, all files owned by Savant Software, LLC
# Software Contact Tadas Tsibulskis, <tadas@savant.capital>

# TODO : remove this ( This fix lets packages be imported from the src folder. )
import sys
sys.path.append(".")

import time
import json
from datetime import datetime
from src.aws_textract import analyze_image
from src.screenshot_util import execute_screenshot
from src.socket_service import udputil
from src.db_service import db_service
from src.log_service import log_service

today = datetime.now()

# CONFIG STUFF
start_delay = 5
capture_interval = 3
ocr_timeout = 10 #hours

if __name__ == "__main__":
    time.sleep(start_delay)
    timeout = time.time() + 60 * 60 * ocr_timeout

    while True:
        if time.time() > timeout:
            break

        now = datetime.now()

        directory_name = today.strftime('%Y-%m-%d')
        file_name = now.strftime("%H:%M:%S")
        image_location = execute_screenshot.capture_screen("/tmp/" + directory_name)

        # Save to S3
        # try :
        #     screenshot_url = db_service.save_image_to_bucket(image_location, directory_name, file_name)
        # except Exception as e:
        #     log_service.report_error(directory_name, "Failed to save screen shot to S3 Bucket", e)
        #     print("Failed to save screen shot to S3 Bucket {0}\n".format(image_location))
        #     print(e)
        #     continue
        #
        # # Try Table Extraction
        # try:
        #     csv_data = analyze_image.get_csv_from_image(image_location)
        # except Exception as e:
        #     log_service.report_error(directory_name, "Failed to parse to csv", e)
        #     print("Failed to parse to csv {0}\n".format(image_location))
        #     print(e)
        #     continue
        #
        # # Convert to JSON
        # try:
        #     dist_data = analyze_image.convert_to_dist(csv_data)
        # except Exception as e:
        #     log_service.report_error(directory_name, "Failed to convert to json", e)
        #     print("Failed to convert to json {0}".format(image_location))
        #     print(e)
        #     continue

        dist_data = [
            {
                "Symbol": "AAPL"
             }
        ]

        try:
            # If we successfully parse the table, push to go process
            for item in dist_data:
                item["CapturedAt"] = now.strftime("%d-%m-%Y %H:%M:%S")
                # item["Location"] = screenshot_url
                udputil.send_message(json.dumps(item))
                log_service.write_to_log(directory_name, item)
                print("Data sent, continuing.")
        except Exception as e:
            log_service.report_error(directory_name, "Failed push data to go process", e)
            print("Failed push data to go process: {0}\n".format(image_location))
            print(e)
            continue

        # Waits before next capture
        time.sleep(capture_interval)
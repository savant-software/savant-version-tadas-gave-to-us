# Copyright (C) Savant Software, LLC - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential, all files owned by Savant Software, LLC
# Software Contact Tadas Tsibulskis, <tadas@savant.capital>

import boto3
import os
from src.log_service import log_service

# TODO : Move these into Vault
os.environ["AWS_ACCESS_KEY_ID"] = "AKIAQVL7BDPUEWJYKDFY"
os.environ["AWS_SECRET_ACCESS_KEY"] = "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"
os.environ["AWS_DEFAULT_REGION"] = "us-east-1"

def save_image_to_bucket(file_location, directory_name, captured_at):
    try:
        data = open(file_location, "rb")
    except FileNotFoundError as e:
        log_service.report_error(directory_name, "Error Saving to S3: Could not locate file", e)

    s3_client = boto3.resource('s3')

    destination = directory_name + "/" + captured_at + ".png"

    # Upload the file to S3
    s3_client.Bucket("ocr-screenshots").put_object(Key=destination, Body=data)
    object_acl = s3_client.ObjectAcl("ocr-screenshots", destination)
    response = object_acl.put(ACL="public-read")

    return destination

import os
import json

def write_to_log(date, item):
    try:
        if not os.path.exists("logs/"):
            os.makedirs("logs/")
        text_file = open("logs/" + date + ".txt", "a")
        text_file.write(json.dumps(item, indent=2, sort_keys=True)+",\n")
        text_file.close()
    except Exception as e:
        print("Failed to write to logs.")

def report_error(date, error, message):
    try:
        if not os.path.exists("logs/"):
            os.makedirs("logs/")
        error_heading = "Error: " + str(error)
        error_message = "Message: " + str(message)
        text_file = open("logs/" + date + ".txt", "a")
        text_file.write("{0} | {1},\n".format(str(error_heading), str(error_message)))
        # text_file.write("{" + error_heading + "\n" + error_message + "},")
        text_file.close()
    except Exception as e:
        print("Failed to write error to logs.")

# Copyright (C) 2019-2021 Savant, LLC - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential, all files owned by Savant, LLC
# Software Contact Michael Esparza, <michael@savant.llc>

import os
import uuid
import time

from PIL import ImageGrab
import pyscreenshot as sc
from src.db_service import db_service


def capture_screen(output_path):
    # Create the temp dir if is does not exist
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    im = sc.grab(bbox=(5, 173, 2530, 770))

    img_path = output_path + "-" + str(uuid.uuid1()) + ".png"
    im.save(img_path)

    # Return absolute path to image
    return img_path

